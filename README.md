# My Website
This is the code for my personal website hosted at [rjlorenzo.me](rjlorenzo.me)

## Stack
- [Hugo](https://gohugo.io/) for static site generation.
- [LoveIt](https://github.com/dillonzq/LoveIt) for the Hugo theme. 
- [Render](https://render.com/) for the static site hosting and SSL.
